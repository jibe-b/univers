---
title:
...

## Problèmes ayant été décrits

Des problèmes ont été décrits, à propos d'éléments de notre univers.

Consulter la [liste des issues](https://gitlab.com/jibe-b/univers/issues)

## Données nécessaires

Des données sont nécessaires pour agir de manière fiable.

Consulter la [liste de données à collecter](collecte/index.html)

