./univers inv http://jibe-b.github.io/resource/sol_champ_2 rdf:type https://en.wikipedia.org/wiki/Soil . sol_champ_2 est un sol
./univers log "http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/ontology/taux_humidite '0pourcent' ." "Le taux d'humidite est egal a 0."
./univers exe http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/ontology/caracteristique
http://jibe-b.github.io/ontology/sec . Avec ce taux d'humidite sol_champ_2 est dans un etat sec
./univers sim a j+10, avec param={"pluie": "15mm"} : http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/taux_humidite "3pourcent" . a 10 jours, le taux d'humidite pourra etre a 3pourcent 

./univers wish http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/caractristique http://jibe-b.github.io/humide . Un sol humide est souhaite
./univers q \[<arrosage aerien 1000L>-<mesure taux_humidite>, <arrosage souterrain 700L>-<mesure taux_humidite>\] Des protocoles permettraient d'atteindre l'etat souhaite
./univers log http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/ontology/taux_humidite '50pourcent' . Le taux d'humidite du sol est desormais a 50pourcent
./univers exe http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/ontology/caracteristique http://jibe-b.github.io/ontology/humide . Avec ce taux d'humidite, sol_champ_2 est dans un etat humide
./univers sim a j+10, avec param={"pluie": "15mm"} : http://jibe-b.github.io/resource/sol_champ_2 http://jibe-b.github.io/taux_humidite "53pourcent" . a 10 jours, le taux d'humidite pourra etre a 53pourcent
./univers wish http://jibe-b.github.io/wish/sol_champ_2_humide http://jibe-b.github.io/ontology/etat_avancement http://jibe-b.github.io/ontology/atteint . Le souhait "Un sol humide est souhaite" est atteint
