# Issues

## Entrer une nouvelle issue

[Liste d'issues](https://gitlab.com/jibe-b/univers/issues/new)

## Consulter les issues existantes

[Liste d'issues](https://gitlab.com/jibe-b/univers/issues)

NB : suivez le template.

## Contribuer à la collection de données

[Page de contribution](collecte/index.html)

## Consulter l'état de la collection de données

[Tableau d'avancement](https://gitlab.com/jibe-b/blob/dashboard/tableau.md)

## Recommendations résultantes

[Liste de recommendations](https://gitlab.com/jibe-b/blob/recommendations/recommandations.md)

